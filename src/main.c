#include "mem_internals.h"
#include "mem.h"
#include "util.h"
#include <sys/mman.h>

struct block_header *get_header(void *ptr) {
    return (struct block_header *) (((uint8_t *) ptr) - offsetof(struct block_header, contents));
}
static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | additional_flags, 0, 0);
}
//Обычное успешное выделение памяти.
void t1() {
    printf("======================\n");
    size_t query = 3*4096;
    heap_init(query);
    printf("1 test start\n");
    void *ptr = _malloc(900);
    if (ptr != NULL) printf("ptr to contents != NULL\n");
    else {
        fprintf(stderr, "ptr to contents == NULL\n");
        return;
    }
    FILE *file = fopen("test1.txt", "w+");
    struct block_header *header = get_header(ptr);
    debug_struct_info(file, header);
    fclose(file);
    printf("1 test end\n");
}

//Освобождение одного блока из нескольких выделенных.
void t2() {
    printf("======================\n");
    printf("2 test start\n");
    size_t query = 7*4096;
    heap_init(query);
    void *ptr1 = _malloc(2*4096);
    void *ptr2 = _malloc(3*4096);
    if (ptr1 != NULL && ptr2 != NULL) printf("ptr to contents != NULL\n");
    else {
        fprintf(stderr, "ptr to contents == NULL\n");
        return;
    }
    struct block_header *header = get_header(ptr1);
    _free(ptr1);
    if (header->is_free) printf("free work!\n");
    else fprintf(stderr,"free doesn't work!\n");
    printf("2 test end\n");
}

//Освобождение двух блоков из нескольких выделенных.
void t3() {
    printf("======================\n");
    printf("3 test start\n");
    size_t query = 11*4096;
    heap_init(query);
    void *ptr1 = _malloc(4096);
    void *ptr2 = _malloc(2*4096);
    void *ptr3 = _malloc(3*4096);
    void *ptr4 = _malloc(4*4096);
    if (ptr1 != NULL && ptr2 != NULL && ptr3 != NULL && ptr4 != NULL) printf("ptr to contents != NULL\n");
    else {
        fprintf(stderr, "ptr to contents == NULL\n");
        return;
    }
    struct block_header *header1 = get_header(ptr1);
    struct block_header *header3 = get_header(ptr3);
    if (header1->is_free || header3->is_free) fprintf(stderr,"free doesn't work!\n");
    else printf("blocks busy\n");
    printf("ptr1 = %p\n",(void *)ptr1);
    printf("ptr3 = %p\n",(void *)ptr3);
    _free(ptr1);
    _free(ptr3);
    if (header1->is_free && header3->is_free) printf("free work!\n");
    else fprintf(stderr,"free doesn't work!\n");
    printf("3 test end\n");
}

//Память закончилась, новый регион памяти расширяет старый.
void t4() {
    printf("======================\n");
    printf("4 test start\n");
    size_t query = 7*4096;
    heap_init(query);
    void *ptr1 = _malloc(4096);
    void *ptr2 = _malloc(2*4096);
    void *ptr3 = _malloc(3*4096);
    void *ptr4 = _malloc(4*4096);

    if (ptr1 != NULL && ptr2 != NULL && ptr3 != NULL && ptr4 != NULL) printf("ptr to contents != NULL\n");
    else {
        fprintf(stderr, "ptr to contents == NULL\n");
        return;
    }
    printf("4 test end\n");
}

//Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов,
// новый регион выделяется в другом месте.
void t5() {
    printf("======================\n");
    printf("5 test start\n");
    size_t query = 8000;
    void * heap_ptr1 = heap_init(query);
    void *ptr1 = _malloc(6000);
    struct block_header *header = (struct block_header *) heap_ptr1;
    while (header->next!=NULL) header = header->next;
    map_pages(header, 1000, MAP_FIXED);
    void *ptr2 = _malloc(4096);
    if (ptr1 != NULL && ptr2 != NULL) printf("ptr to contents != NULL\n");
    else {
        fprintf(stderr, "ptr to contents == NULL\n");
        return;
    }

    printf("5 test end\n");
}

int main() {
    t1();
    t2();
    t3();
    t4();
    t5();
    return 0;
}

